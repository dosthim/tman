# README #

This is a game project which is called Tman . An endless game type which is developing under Phaser.js framework. All the artwork are created and copyrighted. the storyline is Aliens are intruders who wants to catch Tman. Your job is to save Tman from this aliens as long as you can. use your cursor key to manipulate Tman. If Tman collide with alien, his health bar shows life reducing. when life is zero the game is over. 

### What is this repository for? ###

* Tman game development
* Version: 2.0

### How do I get set up? ###

* need a local webserver like moongoose server etc. 
* After setup server clone project to your pc. 
* browse index.html from server.open it in browser.
* Dependencies: phaser.js

### Contribution guidelines ###

* Writing tests: Md. Shiam Shabbir Himel
* Artwork: Zakia Afroz Abedin
* Code review: Md. Shiam Shabbir Himel


### Who do I talk to? ###

* Repo owner or admin: Md. Shiam Shabbir Himel (sabbirshiam@gmail.com)
* Other community or team contact: Zakia Afroz Abedin