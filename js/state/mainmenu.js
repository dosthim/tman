var MainMenu = function(game) {
	this._tman = null;
	this._LastScore = null;
	this._fontStyle = null;
this._fontStyle_menu = null;
this._gameHeading = null;
this.option_start = null;
this.option_option = null;
this.option_exit = null;
this.stand = null;
this.start_background = null;
ToggleSoundImage = null;
mMuteSound = false;
helpImage = null;
score = 0;//total score
	
};

MainMenu.prototype = {
	create : function() {
		this.stage.backgroundColor = '#000000';
		this.start_background = this.add.sprite(0, 0, 'bg_combined');
		//this.start_background.anchor.setTo(0.5, 0.5);
		//this.start_background.alpha = 0;
		this._tman = this.add.sprite(25, 120, 'tizen_man_static');
		// this._tman.anchor.setTo(0.5, 0.5);
		//this._tman.alpha = 0;
		this._tman.scale.x = 0.50;
		this._tman.scale.y = 0.50;
		this._tman.animations.add('idle', [ 0, 1, 2 ], 10, true);
		this._tman.animations.play('idle');
		this._fontStyle = {
			font : "55px Cursive",
			fill : "#945430",
			stroke : "#333",
			strokeThickness : 5,
			align : "center",
            
		};
        
		this._fontStyleScore = {
				font : "25px Cursive",
				fill : "#FFCC00",
				stroke : "#333",
				strokeThickness : 5,
				align : "center"
			};
		this._fontStyle_menu = {
			font : "30px Cursive",
			fill : "#FFEEAA",
			stroke : "#333",
			strokeThickness : 5,
			align : "center"
		};
        this._gameHeading = this.add.image((GAME_WIDTH / 2) - 120, 40,'gametitle');
		/*this._gameHeading = this.add.text((Tman.GAME_WIDTH / 2) - 90, 40,
				"T Man", this._fontStyle);
        this._gameHeading.setShadow(3, 3, 'rgba(255, 255, 255, 0.5)', 0);*/
		this._LastScore = this.add.text((GAME_WIDTH / 2) - 90, 140,
				"Last Score: 0", this._fontStyleScore);
		this._LastScore.setText("Last Score: " + score);
		this.stand = this.add.sprite(520, 90, 'stand');
		this.stand.scale.x = 0.70;
		this.stand.scale.y = 0.60;

		this.option_start = this.add.button(470, 146, 'plank_top',
				this.startGame, this);
		this.option_start.scale.x = 0.70;
		this.option_start.scale.y = 0.70;
		this.option_start.text = this.add.text(510, 150, "start",
				this._fontStyle_menu);

        this.option_option = this.add.button(470, 202, 'plank_mid', this.options, this);
        this.option_option.scale.x = 0.70;
        this.option_option.scale.y = 0.70;
        this.option_option.text = this.add.text(500, 204, "Help",
                this._fontStyle_menu);

		this.option_exit = this.add.button(470, 250, 'plank_bot', this.GameExit, this);
		this.option_exit.scale.x = 0.70;
		this.option_exit.scale.y = 0.70;
		this.option_exit.text = this.add.text(515, 258, "exit",
				this._fontStyle_menu);
		//this.ToggleSoundImage = this.add.button((Tman.GAME_WIDTH / 2)+200, this.game.state.CenterY-100, 'toggleSound', this.ToggleSound, this,0);
		
	},
	startGame : function() {

		/*this.add.tween(this._tman).to({
			alpha : 1
		}, 2000, Phaser.Easing.Linear.None, true, 0, 1000, true);
		this.add.tween(this.start_background).to({
			alpha : 1
		}, 2000, Phaser.Easing.Linear.None, true, 0, 1000, true);*/
		this.state.start('MainIntro');
		//this.state.start('Game');
	},
    options: function(){
    	helpImage = this.add.image(70, 40, 'help');
    	helpImage.inputEnabled = true;
    	helpImage.events.onInputDown.add(this.CloseHelp, this);
    	
    },
    CloseHelp: function(){
    	helpImage.visible = !helpImage.visible;
    },
    
    GameExit: function(){
    	this.game.world.removeAll();
    	//window.close();
    	//tizen.application.getCurrentApplication().exit();
    },
    
    ToggleSound: function(){
    	
    	switch(this.ToggleSoundImage.frame){
    	case 0:
    		this.ToggleSoundImage.kill();
    		this.ToggleSoundImage = this.add.button((Tman.GAME_WIDTH / 2)+200, this.game.state.CenterY-100, 'toggleSound', this.ToggleSound, this,1);
    		mMuteSound = true;
    		break;
    	case 1:
    		this.ToggleSoundImage.kill();
    		this.ToggleSoundImage = this.add.button((Tman.GAME_WIDTH / 2)+200, this.game.state.CenterY-100, 'toggleSound', this.ToggleSound, this,0);
    		mMuteSound = false;
    		break;
    	}
    }
};