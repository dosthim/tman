var game_env;
var cursors;
var bg_image;
var tman_stand;
var tman_fly;
var fireButton;
var number_of_alien = 6;
var game_world_center_x = 0;
var game_world_center_y = 0;
var tmanToggled = 0;
var aliens = null;	//enemy group
var AlienImageArray = [ "allien_two", "allien_three", "allien_four",
			"allien_five", "allien_six", "allien_seven", "allien_eight" ];

var bmpText;
var score;
var isGameStart = false;
var scoreUpdateTimer;
var healthTimer
var healthText;
var timeCheck;
var pauseButton;
var _fontStyle;
var gameOver;
var gameOverTimer;

var Game = function(game) {
	game_env  = game;
	bg_roll_x = 0;
	myHealthBar = null;
	curHealth = 100;
	_fontStyle = {
			font : "40px Arial",
			fill : "#FFCC00",
			stroke : "#333",
			strokeThickness : 5,
			align : "center"
		};
	
};

function startRolling(){
		
		tman_ReadyToFly(true);
		bg_roll_x = 2;
		aliens.setAll('visible',true);
		aliens.forEach(function(alien) {
			if(alien.visible)
				alien.body.velocity.set(game_env.rnd.integerInRange(-200, -100), 0);
		});
		isGameStart = true;
		heart.visible = true;
		heart.body.velocity.set(game_env.rnd.integerInRange(-200, -100), 0);
		scoreUpdateTimer.start();
		
	};
function stopRolling(){
		//bg_image.kill();
		tman_stand.kill();
		tman_fly.kill();	
		aliens.removeAll();
		isGameStart = false;
		//changeState();
		//game_env.state.start('Game');
	};
function tman_ReadyToFly(isready){
	if(isready){
			tmanToggled = 1;
			tman_stand.kill();
			tman_stand.body.enable = false;
			tman_stand.visible = false;
			tman_fly.body.enable = true;
			tman_fly.visible = true;
		}else{
			tmanToggled = 0;
			tman_stand.revive();
			tman_stand.body.enable = true;
			tman_stand.visible = true;
			tman_fly.body.enable = false;
			tman_fly.visible = false;
			tman_fly.kill();
		}
	};
function getCaught(tman,alien) {
	    alien.reset(640, game_env.world.randomY);
		alien.body.velocity.set(game_env.rnd.integerInRange(-200, -100), 0);
		TmanToggled = 0;
		if( curHealth == 0){
			stopRolling();
			gameOverTimer.start();		
		}else{
			if(score<1)
				score = 0;
			else
				score-=10
				curHealth-=10;
		}
		myHealthBar.setPercent(curHealth);
		healthText.text = curHealth +' %';	
	};
function removeAlien(alien){
	    alien.reset(590+game_env.rnd.integerInRange(5, 50), game_env.world.randomY);
		if(score>100)
			alien.body.velocity.set(game_env.rnd.integerInRange(-500, -700), 0);
		if(score>60)
			alien.body.velocity.set(game_env.rnd.integerInRange(-300, -400), 0);
		else
			alien.body.velocity.set(game_env.rnd.integerInRange(-200, -100), 0);
	
};
function managePause() {
		// ...
		game_env.paused = true;
	
		
		var pausedText = game_env.add.text(100, 170,
				"Game paused.\nTap anywhere to continue.", _fontStyle);
		game_env.input.onDown.add(function() {
			pausedText.destroy();
			game_env.paused = false;
		}, this);
	};
function initGameEnv(){
		game_env.physics.startSystem(Phaser.Physics.ARCADE); //start physics engine
		//  We only want world bounds on the left and right
		game_env.physics.setBoundsToWorld();
		 //  Set the world (global) gravity
		game_env.physics.arcade.gravity.y = 30;
		game_world_center_x = game_env.world.centerX;
		game_world_center_y = game_env.world.centerY;
	 	cursors = game_env.input.keyboard.createCursorKeys();//  And some controls to play the game with
		fireButton = game_env.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);
		
		
};
function initBackground(){
	//background creation
		bg_image =  game_env.add.tileSprite(0,0,680, 340, 'bg_night');
		bg_roll_x = 0;
		bg_image.inputEnabled = true;   //Enables all kind of input actions on this image (click, etc)
		//bg_image.events.onInputDown.add(startRolling, this);
		game_env.input.onDown.addOnce(startRolling, this);
};
function initHealthBar(){
	//health bar creation 
		var barConfig = {x: 300, y: 20, width: 220, height: 10};
		myHealthBar = new HealthBar(game_env, barConfig);
		curHealth = 100;
		myHealthBar.setPercent(curHealth); 
		healthText = game_env.add.bitmapText(423, 8, 'desyrel', curHealth+' %', 15);
};
function initEnemyGroup(){
			// // Create a enemies group to store the baddies
		aliens = game_env.add.group();
		aliens.enableBody = true;
		aliens.physicsBodyType = Phaser.Physics.ARCADE;
		
		// Create some enemies.
		for (var i = 0; i < 6; i++)
		{
        // Since the getFirstExists() which we'll use for recycling
        // cannot allocate new objects, create them manually here.
			
			var alien = aliens.create(game_env.world.width-40,game_env.rnd.integerInRange(3, game_env.world.height), AlienImageArray[i]);
			alien.visible = false;
			alien.checkWorldBounds = true;
			
			alien.events.onOutOfBounds.add(removeAlien, this);
		}
		aliens.setAll('body.allowGravity',false);
};
function initTman(){

	//Tman creation
		tman_stand = game_env.add.sprite((game_world_center_x/2)-30,game_world_center_y-60,'t_man_stand');
		game_env.physics.enable(tman_stand, Phaser.Physics.ARCADE);
		tman_stand.scale.x = 0.3;
		tman_stand.scale.y = 0.3;
		tman_stand.anchor.setTo(0.9, 0.8);
		
		tman_stand.body.enable = true;
		tman_stand.body.collideWorldBounds = true;
		//tman_stand.body.bounce.y = 0.8;
		tman_stand.animations.add('idle', [ 0, 1, 2 ], 8, true);
		tman_stand.animations.play('idle');
		tman_stand.body.allowGravity = false;
		tman_stand.body.immovable = true;
		
		tman_fly = game_env.add.sprite((game_world_center_x/2)-30,game_world_center_y-60,'t_man_fly');
		game_env.physics.enable(tman_fly, Phaser.Physics.ARCADE);
		tman_fly.scale.x = 0.7;
		tman_fly.scale.y = 0.7;
		tman_fly.anchor.setTo(0.9, 0.8);
		tman_fly.visible = false;
		tman_fly.body.enable = false;
		tman_fly.body.collideWorldBounds = true;
		//tman_fly.body.bounce.y = 0.8;
		tman_fly.animations.add('idle', [ 0, 1, 2 ], 8, true);
		tman_fly.animations.play('idle');
		tman_fly.inputEnabled = true;   //Enables all kind of input actions on this image (click, etc)
		game_env.camera.follow( tman_fly, Phaser.Camera.FOLLOW_LOCKON );
};

function initScoreText(){
	score = 0;
	isGameStart = false;
	bmpText = game_env.add.bitmapText(20, 10, 'desyrel', 'Score: 0', 30);
	 //  Create our Timer
    scoreUpdateTimer = game_env.time.create(false);

    //  Set a TimerEvent to occur after 2 seconds
    scoreUpdateTimer.loop(300, updateScore, this);

    //  Start the timer running - this is important!
    //  It won't start automatically, allowing you to hook it to button events and the like.
    
};
function updateScore() {
	if( isGameStart )
			bmpText.text = 'Score:' + score++;

};

function removeHeart(heart){
	heart.kill();

};
function createHeart(){

    heart = game_env.add.sprite(game_env.world.width-40,game_env.rnd.integerInRange(3, game_env.world.height), 'heart');
    game_env.physics.enable(heart, Phaser.Physics.ARCADE);
	heart.visible = false;
	heart.body.enable = true;
    heart.body.bounce.y = 0.9;
	heart.body.allowGravity=false;
    heart.body.checkWorldBounds = true;
	heart.events.onOutOfBounds.add(removeHeart, this);
	
	// test for 3 second delay
	timeCheck  = game_env.time.now;
};
function fireHealingHeart(){
	heart.revive();
	heart.reset(590+game_env.rnd.integerInRange(5, 50), game_env.world.randomY);
	heart.body.velocity.set(game_env.rnd.integerInRange(-200, -100), 0);
};
function healTman(heart,tman_fly){
	heart.kill();
	if( curHealth<100 )
	curHealth+=10;
	myHealthBar.setPercent(curHealth);
	healthText.text = curHealth +' %';
	
};
function ChangeState() {
		game_env.state.start('MainMenu');
	};
	
Game.prototype = {
	preload : function() {

		//The Background
		game_env.load.image('bg_night', 'img/building.png');
		game_env.load.image('pause', 'img/pause.png');
		// The villains 
		game_env.load.image('allien_two', 'img/alien-02.png');
		game_env.load.image('allien_three', 'img/alien-03.png');
		game_env.load.image('allien_four', 'img/alien-04.png');
		game_env.load.image('allien_five', 'img/alien-05.png');
		game_env.load.image('allien_six', 'img/alien-06.png');
		game_env.load.image('allien_seven', 'img/alien-07.png');
		game_env.load.image('allien_eight', 'img/alien-08.png');
		//health
		game_env.load.image('heart', 'img/heart.png');
		//the hero
		game_env.load.spritesheet('t_man_stand', 'img/tizenboy.png',168,148, 3);
		game_env.load.spritesheet('t_man_fly', 'img/tizen_boy_fly.png',96,48, 4);
		//the bonus
		game_env.load.spritesheet('coins', 'img/coins.png', 30, 30, 4);
        game_env.load.image('berries', 'img/burger.png');
		
		game_env.load.bitmapFont('desyrel', 'fonts/desyrel.png', 'fonts/desyrel.xml');
	},
	create : function() {
		initGameEnv();
		initBackground();
		initHealthBar();
		initTman();
		initEnemyGroup();
		initScoreText();
		createHeart();
		pauseButton = game_env.add.button(570, 10, 'pause', managePause,
				this);
				//  Create our Timer
		gameOverTimer = game_env.time.create(false);

		//  Set a TimerEvent to occur after 3 seconds
		gameOverTimer.add(100, this.fadePicture, this);
	},
	

	update : function() {
	//  Scroll the background
	
		bg_image.tilePosition.x -= bg_roll_x;
		if ( cursors.left.isDown )
            tman_fly.body.velocity.x = -200;
        else if ( cursors.right.isDown )
			tman_fly.body.velocity.x = 200;
        else if( cursors.up.isDown ) 	
			tman_fly.body.velocity.y = -200;
		else
			tman_fly.body.velocity.y = 200;
			// test for 3 second delay
		if (game_env.time.now - timeCheck > 10000 && isGameStart)
		{
			//20 seconds have elapsed, so safe to do something
			timeCheck = game_env.time.now;
			fireHealingHeart();
		}
		
		game_env.physics.arcade.overlap( aliens, tman_fly, getCaught, null, this );
		game_env.physics.arcade.overlap( heart, tman_fly, healTman, null, this );
	},

	fadePicture : function() {
		var tween;
		gameOver = this.add.tileSprite(0, 0, 680, 340, 'gameover');
		gameOver.alpha = 0;
		tween = game_env.add.tween(gameOver).to({
			alpha : 1
		}, 4000, Phaser.Easing.Linear.None, true);
		game_env.time.events.add(Phaser.Timer.SECOND * 5, ChangeState,
				this);

	},
	//bonus point add
	getCollectionPoint : function() {
		
	},
	render : function() {

    aliens.forEach(function(item) {
	//	if(item.visible)
		//	game_env.debug.body(item);
    });
	/*if(tman_fly.visible)
		game_env.debug.body(tman_fly);
	if(heart.visible)
		game_env.debug.body(heart);*/
	}
		
};



