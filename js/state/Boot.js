//var Tman = {};
var Boot = function(game) {};
Boot.prototype = {
    preload: function() {
	
        this.load.image('preloaderBar', 'img/loading-bar.png');
    },
    create: function() {
        this.input.maxPointers = 1;
        this.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
    //    game.stage.scaleMode = Phaser.StageScaleMode.SHOW_ALL; //resize your window to see the stage resize too
      //  this.scale.setShowAll();
       // this.scale.refresh();
        //this.scale.startFullScreen();
        this.scale.pageAlignHorizontally = true;
        this.scale.pageAlignVertically = true;
        this.scale.setScreenSize(false);
        this.state.start('preload');
    }
};