
var preload = function(game){
    GAME_WIDTH = 680;
    GAME_HEIGHT = 340;
    _scoreText = 0;
};
preload.prototype = {
    preload: function() {
        this.stage.backgroundColor = '#000000';
        var preloadBar = this.add.sprite((GAME_WIDTH-311)/2,
            (GAME_HEIGHT-27)/2, 'preloaderBar');
		preloadBar.anchor.setTo(0.5,0.5);	
        this.load.setPreloadSprite(preloadBar);
 
        
        //miselenious
        this.load.image('Heading', 'img/TMan.png');
        
        //background image 
        this.load.image('bg_combined', 'img/building.png');
        //this.load.image('start_bg', 'img/backg201.png');
        
        //start ,option,exit background image
        this.load.image('plank_bot', 'img/plank_bot.png');
        this.load.image('plank_mid', 'img/plank_mid.png');
        this.load.image('plank_top', 'img/plank_top.png',35,35);
        this.load.image('stand', 'img/stand.png',200,250);
        
        
        
        //sprite sheets
        this.load.spritesheet('tizen_man_static', 'img/tizenboy.png',170,148, 3);
        
        //sound file
      //  this.load.audio('tmanFlap', ['sound/flap.ogg']);
       // this.load.audio('backgroundMusic', ['sound/pappu-pakia2.3.ogg']);
        
        this.load.image('gameover', 'img/game_over.png');
        
        this.load.image('gametitle', 'img/gameheading.png');
        this.load.spritesheet('toggleSound','img/mute.png',51,40,2);
        this.load.image('help', 'img/help.png');
       // this.load.image('IntroBack', 'img/intro_back.png');
        this.load.image('gameintro1', 'img/gameintro1.png');
        this.load.image('gameintro2', 'img/gameintro2.png');
        this.load.image('tmanlaugh', 'img/tmanlaugh.png');
        
    },
    create: function() {
        this.state.start('MainMenu');
		//this.state.start('Game');
    }
};

