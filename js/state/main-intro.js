
var MainIntro = function(game) {
	var IntroBack = null;
	/*var Tman = null;
	//var AlienTwo = null;
	var TmanDialog = null;
	var IntroductionLine = null;
	var GroupAlien = null;
	var GroupAlienLaugh = null;
	var GroupAlienGossip = null;
	var AlienGossipHmm = null;*/
	var StoryLine6 = null;
	//var AlienTwo = null;
	var StoryLine7 = null;
	var gameintro1 = null;
	var StoryLine2 = null;
	var StoryLine5 = null;
	var StoryLine3 = null;
	var StoryLine4 = null;
	
	var timer = null;
};
var current = 2;
var pictureA = null;
MainIntro.prototype = {
    create: function() {
    	this.game.stage.backgroundColor ='#000000';// '#0072bc';
    	
    	//IntroBack = this.add.sprite(Tman.WIDTH, Tman.HEIGHT, 'IntroBack');
    	/*Tman = this.add.sprite((Tman.WIDTH/2)-140, 180, 'tizen_man_static');
    	TmanDialog =this.add.sprite(Tman.x+120, Tman.y-90, 'TmanDialog'); 
    	IntroductionLine = this.add.sprite(this.game.world.centerX, 320, 'IntroductionLine');
    	GroupAlien = this.add.sprite(this.game.world.centerX, 150, 'GroupAlien');
    	GroupAlienLaugh = this.add.sprite(GroupAlien.x+20, GroupAlien.y-80, 'GroupAlienLaugh'); 
    	GroupAlienGossip = this.add.sprite(GroupAlien.x+170, GroupAlien.y-50, 'GroupAlienGossip');
    	AlienGossipHmm = this.add.sprite(GroupAlien.x-100, GroupAlien.y+50, 'AlienGossipHmm');*/
    	gameintro1 = this.add.tileSprite(0,0,680,340, 'gameintro1');
    	gameintro2 = this.add.tileSprite(0,0,680,340, 'gameintro2');
    	gameintro3 = this.add.tileSprite(0,0,680,340, 'tmanlaugh');
    	/*StoryLine7 =this.add.sprite(Tman.x+120, Tman.y-90, 'StoryLine7'); 
    	pictureA = this.add.sprite(this.game.world.centerX, 320, 'StoryLine1');
    	StoryLine2 = this.add.sprite(this.game.world.centerX, 150, 'StoryLine2');
    	StoryLine5 = this.add.sprite(StoryLine2.x+20, StoryLine2.y-80, 'StoryLine5'); 
    	StoryLine3 = this.add.image(StoryLine2.x+170, StoryLine2.y-50, 'StoryLine3');
    	StoryLine4 = this.add.sprite(StoryLine2.x-100, StoryLine2.y+50, 'StoryLine4');*/
    	gameintro1.alpha = 0;
    	gameintro2.alpha = 0;
    	gameintro3.alpha = 0;
    	/*StoryLine2.alpha = 0;
    	StoryLine3.alpha = 0;
    	StoryLine4.alpha = 0;
    	StoryLine5.alpha = 0;
    	StoryLine6.alpha = 0;
    	StoryLine7.alpha = 0;*/
    	/*Tman.alpha = 0;
    	TmanDialog.alpha = 0;
    	IntroductionLine.alpha = 0;
    	GroupAlien.alpha = 0;
    	GroupAlienLaugh.alpha = 0;
    	GroupAlienGossip.alpha = 0;
    	AlienGossipHmm.alpha = 0;*/
    	/*Tman.visible = false;
    	TmanDialog.visible = false;
    	IntroductionLine.visible = false;
    	GroupAlien.visible = false;
    	GroupAlienLaugh.visible = false;
    	GroupAlienGossip.visible = false;
    	AlienGossipHmm.visible = false;*/
        //  Create our Timer
        this.timer = this.game.time.create(false);

        //  Set a TimerEvent to occur after 3 seconds
        this.timer.add(200, this.StoryLine, this);
        this.timer.start();
    },
    StoryLine: function(){
    	 var tween;
         
         tween = this.game.add.tween(gameintro1).to( { alpha: 1 }, 4000, Phaser.Easing.Linear.None, true);
         
        // this.game.add.tween(gameintro2).to( { alpha: 1 }, 4000, Phaser.Easing.Linear.None, true);
         tween.onComplete.add(this.changePicture, this);
        // tween.onComplete.add(this.changePicture, this);
        // this.game.time.events.add(Phaser.Timer.SECOND * 10, this.changeState, this);
    },
    changeState: function(){
    	this.state.start('Game');
    },
    changePicture: function(){
    	var tween;
    	//var text = 'StoryLine' + current;
    	//pictureA.loadTexture(text);
    	 if(current==2){
    		tween = this.game.add.tween(gameintro2).to( { alpha: 1 }, 4000, Phaser.Easing.Linear.None, true);
    		tween.onComplete.add(this.changePicture, this);
    	}else if(current==3){
    		tween = this.game.add.tween(gameintro3).to( { alpha: 1 }, 6000, Phaser.Easing.Linear.None, true);
    		tween.onComplete.add(this.changePicture, this);
    	}else{
    		this.game.time.events.add(Phaser.Timer.SECOND * 2, this.changeState, this);
    	}
    	current++;
    }

};