Tman.Game = function(game) {

	this._tman = null;
	var _player = null;
	this._alienGroup = null;
	this._spawnalienTimer = 0;
	this._fontStyle = null;
	Tman._scoreText = null;
	Tman._score = 0;
	Tman._health = 0;
	this._background = null;
	this.input = null;
	this.TmanToggled = 0;

	//alien related variables
	this.number_of_alien = 6;
	var alien_one_sprite = null;
	var _alienGroupOne = null;
	this.minSpeed = 0;
	this.maxSpeed = 0;
	this.vx = 0;
	this.vy = 0;
	this.x = 0;
	this.y = 0;
	this.AlienImageArray = [ "allien_two", "allien_three", "allien_four",
			"allien_five", "allien_six", "allien_seven", "allien_eight" ];

	//bonusPoint related variables
	this._bonusPointGroup = null;
	this._spawnbonusPointTimer = 0;

	this.number_of_bonusPoint = 6;
	this.bonusPoint_one_sprite = null;
	this._bonusPointGroupOne = null;
	this.bonusPointImageArray = [ "berries", "coins" ];
	this.isCollide_BonusPoint = null;//initialization

	//audio
	this.flap = null;
	this.background = null;
	gameOver = null;
	timer = null;

	pauseButton = null;
};
Tman.Game.prototype = {
	create : function() {

		this.physics.startSystem(Phaser.Physics.ARCADE);
		this.physics.arcade.gravity.x = 10;
		this._background = this.add.tileSprite(0, 0, 680, 340, 'bg_combined');

		//Tizen Man object
		_player = this.add.sprite(50, 200, 'tizen_man_static');
		_player.scale.x = 0.50;
		_player.scale.y = 0.50;
		_player.animations.add('idle', [ 0, 1, 2 ], 10, true);
		_player.animations.play('idle');
		_player.inputEnabled = true;
		_player.events.onInputDown.add(this.toggleTman, this);

		this._spawnalienTimer = 0;
		Tman._health = 10;

		this._fontStyle = {
			font : "40px Arial",
			fill : "#FFCC00",
			stroke : "#333",
			strokeThickness : 5,
			align : "center"
		};
		Tman._scoreText = this.add.text(120, 20, "0", this._fontStyle);
		this.pauseButton = this.add.button(570, 20, 'pause', this.managePause,
				this);
		;
		//  Here we create 1 new groups
		_alienGroupOne = this.add.group();
		_alienGroupOne.enableBody = true;
    _alienGroupOne.physicsBodyType = Phaser.Physics.ARCADE;
	//	_alienGroupOne.body.enable=true;
		//create function bonuspoint related codes
		this._spawnbonusPointTimer = 0;
		this._bonusPointGroupOne = this.add.group();
		this._bonusPointGroupOne.enableBody = true;

		/*We add our sounds*/
		this.flap = this.add.audio('tmanFlap');
		this.background = this.add.audio('backgroundMusic');
		this.background.play();

		//  Create our Timer
		this.timer = this.game.time.create(true);

		//  Set a TimerEvent to occur after 3 seconds
		this.timer.add(100, this.fadePicture, this);

	},
	managePause : function() {
		// ...
		this.game.paused = true;
	//	this.flap.pause();
		this.background.pause();
		var pausedText = this.add.text(100, 170,
				"Game paused.\nTap anywhere to continue.", this._fontStyle);
		this.input.onDown.add(function() {
			pausedText.destroy();
			this.game.paused = false;
			//this.flap.resume();
			this.background.resume();
		}, this);
	},
	update : function() {

		if (this.TmanToggled) {
			//this.flap.play();
			this._background.tilePosition.x -= 2;
			//  And enable the Sprite to have a physics body:
			this.physics.arcade.enable(_player);
			_player.anchor.set(0.5, 0.5);
			//  If the sprite is > 8px away from the pointer then let's move to it
			if (this.physics.arcade.distanceToPointer(_player,
					this.input.activePointer) > 0.5) {
				//  Make the object seek to the active pointer (mouse or touch).
				this.physics.arcade.moveToPointer(_player, 200);
			} else {
				//  Otherwise turn off velocity because we're close enough to the pointer
				_player.body.velocity.set(0);
			}

			this._spawnalienTimer += this.time.elapsed;
			if (this._spawnalienTimer > 4000) {
				this._spawnalienTimer = 0;
				Tman.item.spawnAlien(this);
			}
			;

			//update function bonuspoint related codes
			/*this._spawnbonusPointTimer += this.time.elapsed;
			if (this._spawnbonusPointTimer > 10000) {
				this._spawnbonusPointTimer = 0;
				Tman.item.spawnbonusPoint(this); // bonusPoints are called from here
			}
			;*/

			var isCollide = this.physics.arcade.collide(_player,
					_alienGroupOne, this.getCaught, null, this);
			/*this.isCollide_BonusPoint = this.physics.arcade.collide(
					_player, this._bonusPointGroupOne,
					this.getCollectionPoint, null, this);*/
			// Update score
			if (!isCollide) {
				Tman._scoreText.setText(parseInt(Tman._scoreText.text) + 1);

			}

		}

		else
			this._background.tilePosition.x = 0;

		/*if(!Tman._health) {
		    this.add.sprite((Tman.GAME_WIDTH-594)/2, (Tman.GAME_HEIGHT-271)/2, 'game_over');
		    this.game.paused = true;
		    }*/
	},
	getCaught : function() {
		//console.log("Collision detected");
		this.TmanToggled = 0;
		score = parseInt(Tman._scoreText.text);//total score to be shown is assigned to a global variable score
		this.timer.start();
	},
	fadePicture : function() {
		var tween;
		this.gameOver = this.add.tileSprite(0, 0, 680, 340, 'gameover');
		this.gameOver.alpha = 0;
		tween = this.game.add.tween(this.gameOver).to({
			alpha : 1
		}, 4000, Phaser.Easing.Linear.None, true);
		this.game.time.events.add(Phaser.Timer.SECOND * 5, this.ChangeState,
				this);
		//this.game.world.removeAll();

	},
	ChangeState : function() {
		this.game.state.start('MainMenu');
	},
	toggleTman : function() {
		_player.kill();
		_player = this.add
				.sprite(70, 150, 'tizenboy_fly', this.toggleTman);
		_player.animations.add('fly', [ 0, 1, 2, 3 ], 10, true);
		_player.animations.play('fly');
		this.flap.play();

		this.TmanToggled = 1;
	},

	//bonus point add
	getCollectionPoint : function(_player, bonus) {
		if (!this.isCollide_BonusPoint) {
			bonus.kill();
			Tman._scoreText.setText(parseInt(Tman._scoreText.text) + 100);
			//this.removebonusPoint(this._bonusPointGroupOne);
			//removebonusPoint(this.bonusPoint_one_sprite);
		}
	},
	render : function() {

		//this.game.debug.text('Click to disable body1', 32, 32);

	//	if (this.game._player!=null)
		{
			this.game.debug.body(_player);
			this.game.debug.body(_alienGroupOne);
		}
	}	
};

Tman.item = {
	spawnAlien : function(game) {
		//alien enemies
		this.minSpeed = -75;
		this.maxSpeed = 75;
		this.x = 700;//to create aliens from front side of the screen Math.floor(Math.random()*Tman.GAME_WIDTH) + 100;
		this.y = Math.floor(Math.random() * Tman.GAME_HEIGHT);
		this.vx = Math.random() * (this.maxSpeed - this.minSpeed + 1)
				- this.minSpeed;
		var randomnumber = Math.floor(Math.random() * 8);
		// this.vy = Math.random()*(this.maxSpeed - this.minSpeed+1)-this.minSpeed;

		alien_one_sprite = game.add.sprite(this.x, this.y,
				game.AlienImageArray[randomnumber]);
		game.physics.enable(alien_one_sprite, Phaser.Physics.ARCADE);
		alien_one_sprite.body.enable = true;
		alien_one_sprite.anchor.setTo(0.5, 0.5);
		alien_one_sprite.body.checkWorldBounds = true;
		alien_one_sprite.events.onOutOfBounds.add(this.removeAlien, this);
		//alien_one_sprite.body.bounce.setTo(1, 1);
		alien_one_sprite.body.gravity.set(0);
		//alien_one_sprite.body.gravity.x = -5;
		alien_one_sprite.body.velocity.x -= (this.vx);
		alien_one_sprite.body.velocity.y = 0;
		//alien_one_sprite.body.immovable = true;
		_alienGroupOne.add(alien_one_sprite, false);

	},
	removeAlien : function(alien) {
		// ...

		alien.kill();
		// alien._health -= 10;
	},

	//Tman.item added bonuspoint related function... each function just needs a comma to be separated
	spawnbonusPoint : function(game) {
		// ...
		//bonusPoint enemies
		this.minSpeed = -75;
		this.maxSpeed = 75;
		this.x = 700;//Math.floor(Math.random(300,600)*Tman.GAME_WIDTH) + 100;
		this.y = Math.floor(Math.random() * Tman.GAME_HEIGHT);
		this.vx = Math.random() * (this.maxSpeed - this.minSpeed + 1)
				- this.minSpeed;
		var randomnumber = Math.floor(Math.random() * 2);
		// this.vy = Math.random()*(this.maxSpeed - this.minSpeed+1)-this.minSpeed;
		if (randomnumber == 0) // berries
			this.bonusPoint_one_sprite = game.add.sprite(this.x, this.y,
					game.bonusPointImageArray[randomnumber]);
		else //coins
		{
			this.bonusPoint_one_sprite = game.add.sprite(this.x, this.y,
					game.bonusPointImageArray[randomnumber], game.rnd
							.integerInRange(0, 3));
		}
		game.physics.enable(this.bonusPoint_one_sprite, Phaser.Physics.ARCADE);
		this.bonusPoint_one_sprite.anchor.setTo(0.5, 0.5);
		this.bonusPoint_one_sprite.body.checkWorldBounds = true;
		this.bonusPoint_one_sprite.events.onOutOfBounds.add(
				this.removebonusPoint, this);

		//this.game.physics.arcade.collide(_player, this.bonusPoint_one_sprite, this.removebonusPoint);   
		this.bonusPoint_one_sprite.body.gravity.set(0);
		//this.bonusPoint_one_sprite.body.gravity.x = -5;
		this.bonusPoint_one_sprite.body.velocity.x -= (this.vx);
		this.bonusPoint_one_sprite.body.velocity.y = 0;
		//this.bonusPoint_one_sprite.body.immovable = true;
		game._bonusPointGroupOne.add(this.bonusPoint_one_sprite);

	},
	removebonusPoint : function(bonusPoint) {
		// ...
		//bonusPoint.kill();
		this.bonusPoint.kill();
		// bonusPoint._health -= 10;
	}
};

